
import java.util.*;

public class THEOXGAME {

    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char turn = 'O';
    static int count = 0;
    static Scanner input = new Scanner(System.in);

    public static void showWelcome() {
        System.out.println("Hello OX Game");

    }

    public static void showTable() {
        for (int i = 0; i < 3; i++) {
            System.out.print((i + 1) + " ");
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void showTurn() {
        if (turn == 'O') {
            System.out.println("Turn O");
        } else {
            System.out.println("Turn X");
        }
    }

    public static void inputRowCol() {
        System.out.print(turn +"\n"+ "Input row and column : ");
        int row = input.nextInt();
        int col = input.nextInt();
        for (;;) {
            if (row < 1 || row > 3 || col < 1 || col > 3) {
                showTable();
                System.out.println("Sorry invalid input ,Please input row and column again.");
                System.out.print(turn +"\n"+ "Input row and column : ");
                row = input.nextInt();
                col = input.nextInt();
            } else if (!(table[row - 1][col - 1] == '-')) {
                showTable();
                System.out.println("Sorry invalid input ,Please input row and column again.");
                System.out.print(turn +"\n"+ "Input row and column : ");
                row = input.nextInt();
                col = input.nextInt();
            } else {
                break;
            }
        }
        table[row - 1][col - 1] = turn;
        count++;
    }

    public static boolean checkWin() {
        if (table[0][0] == turn && table[0][1] == turn && table[0][2] == turn) {
            System.out.println("The winner is "+turn+".");
            System.out.println("Congratulatins");
            return true;
        } else if (table[1][0] == turn && table[1][1] == turn && table[1][2] == turn) {
            System.out.println("The winner is "+turn+".");
            System.out.println("Congratulatins");
            return true;
        } else if (table[2][0] == turn && table[2][1] == turn && table[2][2] == turn) {
            System.out.println("The winner is "+turn+".");
            System.out.println("Congratulatins");
            return true;
        } else if (table[0][0] == turn && table[1][0] == turn && table[2][0] == turn) {
            System.out.println("The winner is "+turn+".");
            System.out.println("Congratulatins");
            return true;
        } else if (table[0][1] == turn && table[1][1] == turn && table[2][1] == turn) {
            System.out.println("The winner is "+turn+".");
            System.out.println("Congratulatins");
            return true;
        } else if (table[0][2] == turn && table[1][2] == turn && table[2][2] == turn) {
            System.out.println("The winner is "+turn+".");
            System.out.println("Congratulatins");
            return true;
        } else if (table[0][0] == turn && table[1][1] == turn && table[2][2] == turn) {
            System.out.println("The winner is "+turn+".");
            System.out.println("Congratulatins");
            return true;
        } else if (table[0][2] == turn && table[1][1] == turn && table[2][0] == turn) {
           System.out.println("The winner is "+turn+".");
            System.out.println("Congratulatins");
            return true;
        } else {
            return false;
        }
    }
    
    public static boolean checkDraw() {
        if (count == 9) {
            System.out.println("The game is Draw.");
            return true;
        } else {
            return false;
        }
    }

    public static void switchTurn() {
        if (turn == 'O') {
            turn = 'X';
        } else {
            turn = 'O';
        }
    }

    public static void main(String[] arge) {
        showWelcome();
        showTable();
        showTurn();
        for (;;) {
            if (turn == 'O') {
                inputRowCol();
                showTable();
                if (checkDraw()) {
                    break;
                }
                if (checkWin()) {
                    break;
                }
            }
            switchTurn();
            if (turn == 'X') {
                inputRowCol();
                showTable();
                if (checkDraw()) {
                    break;
                }
                if (checkWin()) {
                    break;
                }
            }
        }

    }

}
